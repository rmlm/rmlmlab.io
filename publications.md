---
title: Publications
layout: page
permalink: /publications/
---

Here are lists of my academic works, organized into:
* electronic [Preprints](#preprints)
* peer reviewed [Journal Articles](#articles)
* refereed [Conference Proceedings](#proceedings)

Publications with a free version hosted on the [arXiv](https://arxiv.org/) are
tagged with a <i class="ai ai-arxiv"></i> and the article identifier.
Entries where the published version is
[open access](https://en.wikipedia.org/wiki/Open_access) are indicated with a
<i class="ai ai-open-access" style="color:orangered"></i>.

---

### <a name="preprints">Preprints</a>

{% include_relative publications/rmlm_preprint.html %}

---

### <a name="articles">Journal Articles</a>

{% include_relative publications/rmlm_article.html %}

---

### <a name="proceedings">Conference Proceedings</a>

{% include_relative publications/rmlm_proceeding.html %}

---
