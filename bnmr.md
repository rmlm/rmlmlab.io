---
title: β-NMR
layout: page
permalink: /bnmr/
---

<img src="/uploads/triumf-isac-facility.jpg" title="TRIUMF's ISAC facility" width="100%"/>

β-detected nuclear magnetic resonance (β-NMR) is a sensitive spectroscopic technique
well-suited for interrogating matter at the microscopic level. It is quite akin
to "conventional" magnetic resonance techniques (e.g., NMR, MRI, etc.), but uses
the high-energy β-emissions from radioactive probe nuclei for signal detection.
While the requirement of a radioactive source limits implementations to specialized
facilities (e.g., particle accelerators or nuclear reactors), the technique is
uniquely able to probe material environments that are inaccessible through other
means of study.

A modern implementation of the β-NMR technique can be found at [TRIUMF's](https://www.triumf.ca/)
ISAC facility in [Vancouver, BC](https://vancouver.ca/). Additional information
can be found at [http://bnmr.triumf.ca](http://bnmr.triumf.ca). An unabridged
compilation of publications detailing the experiments performed there (mostly on
materials science) is given below. The list is organized into the following
catergories: 
* [Preprints](#preprints)
* [Journal Articles](#articles)
* [Conference Proceedings](#proceedings)
* [Theses](#theses)

The entries are sorted in reverse chronological order from the date of (online)
publication.

---

## <a name="preprints">Preprints</a>

{% include_relative publications/bnmr_preprint.html %}

---

## <a name="articles">Journal Articles</a>

{% include_relative publications/bnmr_article.html %}

---

## <a name="proceedings">Conference Proceedings</a>

{% include_relative publications/bnmr_proceeding.html %}

---

## <a name="theses">Theses</a>

{% include_relative publications/bnmr_thesis.html %}

---
