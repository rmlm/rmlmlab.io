# rmlm.gitlab.io

This is the source for my GitLab page [rmlm.gitlab.io](https://rmlm.gitlab.io).
The the content and layout here are largely experimental and mainly a playground for
me to try/learn some new things.

The web design used here is based on Joseph Wright's blog
[texdev.net](https://www.texdev.net/), whose source can be found on
[GitHub](https://github.com/josephwright/josephwright.github.io).
Joseph's design work is based on that used by the
[LaTeX Project](https://www.latex-project.org), which was carried out by
[Jonas Jacek](http://jonas.me/), and is licensed under the
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
