# formatted using black

from bibliography import bib

# manuscript preprints on the arXiv
preprints = [
    bib(
        author="R. M. L. McFadden, A. Chatzichristos, D. L. Cortie, D. Fujimoto, Y. S. Hor, H. Ji, V. L. Karner, R. F. Kiefl, C. D. P. Levy, R. Li, I. McKenzie, G. D. Morris, M. R. Pearson, M. Stachura, R. J. Cava, and W. A. MacFarlane",
        title="Local electronic and magnetic properties of the doped topological insulators Bi<sub>2</sub>Se<sub>3</sub>:Ca and Bi<sub>2</sub>Te<sub>3</sub>:Mn investigated using ion-implanted <sup>8</sup>Li β-NMR",
        year="2019",
        month="11",
        day="27",
        arxiv="1911.12212",
        arxiv_cat="cond-mat.mtrl-sci",
        kind="preprint",
        sub_date="2019-11-27",
        pub_date="2019-11-27",
    ),
]
