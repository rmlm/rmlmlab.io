---
title: Hello, world!
layout: post
permalink: /2019/02/01/hello-world.html
---

The canonical first program (here in python):

{% highlight python linenos%}
#!/usr/bin/python3

print('Hello, world!')
{% endhighlight %}

In this case, its also the first post!
