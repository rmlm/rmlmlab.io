---
title: The great experiment
layout: post
permalink: /2019/02/02/the-great-experiment.html
---

This website is an experiment.
The hypothesis is I will have some fun and learn a few things along the way.

So, in the words of Dr. Tobias Fünke, a man self-decribed as a professional twice over: "Let the great experiment begin!"

<center>
   <img src="/uploads/ad-s02e01-tobias.jpg" title="&quot;Let the great experiment begin!&quot;" width="500"/>
</center>

(Arrested Development, S02E01)
