---
title: Experiments
layout: post
permalink: /2019/02/09/experiments.html
---

Here are some experiments at [TRIUMF](https://www.triumf.ca/) that I have been involved with.

The entries have been scraped from the list found [here](https://mis.triumf.ca/science/experiment/list.jsf?schedule=View+all&discipline=View+all&status=View+all).

---

{% include_relative triumf_experiments.html %}

---
