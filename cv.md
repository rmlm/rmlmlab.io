---
title: CV
layout: page
permalink: /cv/
---

I am a Ph.D. candidate in the [Department of Chemistry](https://www.chem.ubc.ca/)
at the [University of British Columbia](https://www.ubc.ca/). I also have 
affiliation with the [Stewart Blusson Quantum Matter Institute](https://qmi.ubc.ca/).

---

* [Education](#education)
* [Awards & Honours](#awards)
* [Service & Membership](#service)
* [Publications](#publications)
   * [Preprints](#preprints)
   * [Journal Articles](#articles)
   * [Conference Proceedings](#proceedings)
   * [Theses](#theses)

---

## <a name="edu">Education</a>

<table id="experiments">
   <thead>
      <th>Year</th>
      <th>Degree</th>
      <th>Institute</th>
      <th>Location</th>
   </thead>
   <tbody>
      <tr>
         <td>2020</td>
         <td>Ph.D.</td>
         <td><a href="https://www.ubc.ca/">University of British Columbia</a></td>
         <td><a href="https://vancouver.ca/">Vancouver, BC</a></td>
      </tr>
      <tr>
         <td>2013</td>
         <td>B.Sc. (Hons.)</td>
         <td><a href="https://www.mta.ca/">Mount Allison University</a></td>
         <td><a href="https://sackville.com/">Sackville, NB</a></td>
      </tr>
   </tbody>
</table>

---

## <a name="awards">Awards & Honours</a>

---

## <a name="service">Service & Membership</a>

<table id="experiments">
   <thead>
      <th>Year</th>
      <th>Position</th>
      <th>Event/Organization</th>
   </thead>
   <tbody>
      <tr>
         <td>2014-2020</td>
         <td>Student Representative</td>
         <td><a href="http://www.isosim.ubc.ca/">IsoSiM</a> Program Committee</td>
      </tr>
      <tr>
         <td>2018</td>
         <td>Canadian Representative</td>
         <td><a href="https://www.iaea.org/">IAEA</a> Technical Meeting <a href="https://www.iaea.org/events/iaea-technical-meeting-on-novel-multidisciplinary-applications-with-unstable-ion-beams-and-complementary-techniques">EVT1703385</a></td>
      </tr>
      <tr>
         <td>2016-2017</td>
         <td>Science Ambassador</td>
         <td><a href="https://www.triumf.ca/">TRIUMF</a></td>
      </tr>
      <tr>
         <td>2015</td>
         <td>Member</td>
         <td><a href="http://www.isosim.ubc.ca/">IsoSiM</a> Summer School Organizing Committee</td>
      </tr>
      <tr>
         <td>2014</td>
         <td>Member</td>
         <td><a href="https://www.cheminst.ca/about/about-csc/">Canadian Society for Chemistry</a></td>
      </tr>
      <tr>
         <td>2007-2012</td>
         <td>Member</td>
         <td><a href="https://www.mta.ca">MtA</a> Chemistry & Biochemistry Society</td>
      </tr>
   </tbody>
</table>

---

## <a name="publications">Publications</a>
Here are lists of my academic works, organized into:
* electronic [Preprints](#preprints)
* peer reviewed [Journal Articles](#articles)
* refereed [Conference Proceedings](#proceedings)

Publications with a free version hosted on the [arXiv](https://arxiv.org/) are
tagged with a <i class="ai ai-arxiv"></i> and the article identifier.
Entries where the published version is
[open access](https://en.wikipedia.org/wiki/Open_access) are indicated with a
<i class="ai ai-open-access" style="color:orangered"></i>.

### <a name="preprints">Preprints</a>

{% include_relative publications/rmlm_preprint.html %}

### <a name="articles">Journal Articles</a>

{% include_relative publications/rmlm_article.html %}

### <a name="proceedings">Conference Proceedings</a>

{% include_relative publications/rmlm_proceeding.html %}

### <a name="theses">Theses</a>

{% include_relative publications/rmlm_thesis.html %}

---
