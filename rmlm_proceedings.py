# formatted using black

from bibliography import bib

# peer reviewed conference proceedings
proceedings = [
    bib(
        author="A. Chatzichristos, R. M. L. McFadden, V. L. Karner, D. L. Cortie, C. D. P. Levy, W. A. MacFarlane, G. D. Morris, M. R. Pearson, Z. Salman, and R. F. Kiefl",
        title="Comparison of <sup>8</sup>Li and <sup>9</sup>Li spin relaxation in SrTiO<sub>3</sub> and Pt: a means to distinguish magnetic and electric quadrupolar sources of relaxation",
        journal="JPS Conf. Proc.",
        volume="21",
        pages="011048",
        year="2018",
        month="03",
        day="01",
        doi="10.7566/JPSCP.21.011048",
        open_access=True,
        kind="proceeding",
        sub_date="2017-06-19",
        pub_date="2018-03-01",
    ),
    bib(
        author="R. M. L. McFadden, A. Chatzichristos, M. H. Dehn, D. Fujimoto, H. Funakubo, A. Gottberg, T. Hitosugi, V. L. Karner, R. F. Kiefl, M. Kurokawa, J. Lassen, C. D. P. Levy, R. Li, G. D. Morris, M. R. Pearson, S. Shiraki, M. Stachura, J. Sugiyama, D. M. Szunyogh, and W. A. MacFarlane",
        title="On the use of <sup>31</sup>Mg for β-detected NMR studies of solids",
        journal="JPS Conf. Proc.",
        volume="21",
        pages="011047",
        year="2018",
        month="03",
        day="01",
        doi="10.7566/JPSCP.21.011047",
        open_access=True,
        kind="proceeding",
        sub_date="2017-06-15",
        pub_date="2018-03-01",
        selected=True,
    ),
    bib(
        author="A. C. Y. Fang, M. H. Dehn, R. M. L. McFadden, V. L. Karner, J. E. Sonier, W. A. MacFarlane, G. D. Morris, C. Gomez, K. Akintola, and R. F. Kiefl",
        title="Origin of the multi-peak muon frequency spectrum in the heavy fermion compound UBe<sub>13</sub>",
        journal="JPS Conf. Proc.",
        volume="21",
        pages="011029",
        year="2018",
        month="03",
        day="01",
        doi="10.7566/JPSCP.21.011029",
        open_access=True,
        kind="proceeding",
        sub_date="2017-06-22",
        pub_date="2018-03-01",
    ),
    bib(
        author="V. L. Karner, R. M. L. Mcfadden, M. H. Dehn, D. Fujimoto, A. Chatzichristos, G. D. Morris, M. R. Pearson, C. D. P. Levy, A. Reisner, L. H. Tjeng, R. F. Kiefl, and W. A. Macfarlane",
        title="Beta-detected NMR of LSAT and YSZ",
        journal="JPS Conf. Proc.",
        volume="21",
        pages="011024",
        year="2018",
        month="03",
        day="01",
        doi="10.7566/JPSCP.21.011024",
        open_access=True,
        kind="proceeding",
        sub_date="2017-06-21",
        pub_date="2018-03-01",
    ),
    bib(
        author="V. L. Karner, R. M. L. Mcfadden, A. Chatzichristos, G. D. Morris, M. R. Pearson, C. D. P. Levy, Z. Salman, D. L. Cortie, R. F. Kiefl, and W. A. Macfarlane",
        title="Beta detected NMR of LaAlO<sub>3</sub>",
        journal="JPS Conf. Proc.",
        volume="21",
        pages="011023",
        year="2018",
        month="03",
        day="01",
        doi="10.7566/JPSCP.21.011023",
        open_access=True,
        kind="proceeding",
        sub_date="2017-06-21",
        pub_date="2018-03-01",
    ),
    bib(
        author="V. L. Karner, T. Liu, I. Mckenzie, A. Chatzichristos, D. L. Cortie, G. D. Morris, R. F. Kiefl, R. M. L. Mcfadden, Z. Fakhraai, M. Stachura, and W. A. Macfarlane",
        title="Exploring the dynamics of glasses using beta detected NMR",
        journal="JPS Conf. Proc.",
        volume="21",
        pages="011022",
        year="2018",
        month="03",
        day="01",
        doi="10.7566/JPSCP.21.011022",
        open_access=True,
        kind="proceeding",
        sub_date="2017-06-21",
        pub_date="2018-03-01",
    ),
    bib(
        author="J. Sugiyama, I. Umegaki, S. Shiraki, T. Hitosugi, R. M. L. McFadden, D. Wang, V. Karner, G. D. Morris, W. A. MacFarlane, and R. F. Kiefl",
        title="Challenge for detecting the interface between electrode and electrolyte with β-NMR",
        journal="JPS Conf. Proc.",
        volume="21",
        pages="011021",
        year="2018",
        month="03",
        day="01",
        doi="10.7566/JPSCP.21.011021",
        open_access=True,
        kind="proceeding",
        sub_date="2017-06-14",
        pub_date="2018-03-01",
    ),
    bib(
        author="W. A. MacFarlane, K. H. Chow, M. D. Hossain, V. L. Karner, R. F. Kiefl, R. M. L. McFadden, G. D. Morris, H. Saadaoui, and Z. Salman",
        title="The spin relaxation of <sup>8</sup>Li<sup>+</sup> in gold at low magnetic field",
        journal="JPS Conf. Proc.",
        volume="21",
        pages="011020",
        year="2018",
        month="03",
        day="01",
        doi="10.7566/JPSCP.21.011020",
        open_access=True,
        kind="proceeding",
        sub_date="2017-06-23",
        pub_date="2018-03-01",
    ),
    bib(
        author="M. Stachura, R. M. L. McFadden, A. Chatzichristos, M. H. Dehn, A. Gottberg, L. Hemmingsen, A. Jancso, V. L. Karner, R. F. Kiefl, F. H. Larsen, J. Lassen, C. D. P. Levy, R. Li, W. A. MacFarlane, G. D. Morris, S. Pallada, M. R. Pearson, D. Szunyogh, P. W. Thulstrup, and A. Voss",
        title="Towards <sup>31</sup>Mg-β-NMR resonance linewidths adequate for applications in magnesium chemistry",
        journal="Hyperfine Interact.",
        volume="238",
        pages="32",
        year="2017",
        month="02",
        day="14",
        doi="10.1007/s10751-017-1408-8",
        open_access=False,
        kind="proceeding",
        sub_date="2017-02-14",
        pub_date="2017-02-14",
    ),
    bib(
        author="C. D. P. Levy, M. R. Pearson, M. H. Dehn, V. L. Karner, R. F. Kiefl, J. Lassen, R. Li, W. A. MacFarlane, R. M. L. McFadden, G. D. Morris, M. Stachura, A. Teigelhöfer, and A. Voss",
        title="Development of a polarized <sup>31</sup>Mg<sup>+</sup> beam as a spin-1/2 probe for BNMR",
        journal="Hyperfine Interact.",
        volume="237",
        pages="162",
        year="2016",
        month="11",
        day="22",
        doi="10.1007/s10751-016-1372-8",
        open_access=False,
        kind="proceeding",
        sub_date="2016-11-22",
        pub_date="2016-11-22",
    ),
    bib(
        author="W. A. MacFarlane, T. J. Parolin, D. L. Cortie, K. H. Chow, M. D. Hossain, R. F. Kiefl, C. D. P. Levy, R. M. L. McFadden, G. D. Morris, M. R. Pearson, H. Saadaoui, Z. Salman, Q. Song, and D. Wang",
        title="<sup>8</sup>Li<sup>+</sup> β-NMR in the cubic insulator MgO",
        journal="J. Phys.: Conf. Ser.",
        volume="551",
        pages="012033",
        year="2014",
        month="12",
        day="16",
        doi="10.1088/1742-6596/551/1/012033",
        open_access=True,
        kind="proceeding",
        sub_date="2014-10-21",
        pub_date="2014-12-16",
    ),
    bib(
        author="R. M. L. McFadden, D. L. Cortie, D. J. Arseneau, T. J. Buck, C.-C. Chen, M. H. Dehn, S. R. Dunsiger, R. F. Kiefl, C. D. P. Levy, C. Li, G. D. Morris, M. R. Pearson, D. Samuelis, J. Xiao, J. Maier, and W. A. MacFarlane",
        title="β-NMR of <sup>8</sup>Li<sup>+</sup> in rutile TiO<sub>2</sub>",
        journal="J. Phys.: Conf. Ser.",
        volume="551",
        pages="012032",
        year="2014",
        month="12",
        day="16",
        doi="10.1088/1742-6596/551/1/012032",
        open_access=True,
        kind="proceeding",
        sub_date="2014-10-21",
        pub_date="2014-12-16",
    ),
    bib(
        author="D. Cortie, T. Buck, R. M. L. McFadden, J. Xiao, C. D. P. Levy, M. Dehn, M. Pearson, G. D. Morris, S. R. Dunsiger, R. F. Kiefl, F. J. Rueß, A. Fuhrer, and W. A. MacFarlane",
        title="β-NMR study of a buried Mn δ-doped layer in a silicon host",
        journal="J. Phys.: Conf. Ser.",
        volume="551",
        pages="012023",
        year="2014",
        month="12",
        day="16",
        doi="10.1088/1742-6596/551/1/012023",
        open_access=True,
        kind="proceeding",
        sub_date="2014-10-21",
        pub_date="2014-12-16",
    ),
]
