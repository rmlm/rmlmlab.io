# formatted using black

from bibliography import bib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FormatStrFormatter

from rmlm_articles import articles
from rmlm_preprints import preprints
from rmlm_proceedings import proceedings
from rmlm_theses import theses

# combined list of all publications
publications = articles + preprints + proceedings + theses


# sorting lists inplace
# https://stackoverflow.com/a/403426
# https://stackoverflow.com/a/4233482

# sort articles into reverse chronological order and desceding page number
# (i.e., for conference proceedings that are published on the same day)
publications.sort(key=lambda p: (p.date.isoformat(), p.pages), reverse=True)

my_name = ["R. M. L. McFadden", "R. M. L. Mcfadden"]
kinds = ["article", "proceeding", "preprint", "thesis"]
# write the sorted/formatted publication lists to files
for k in kinds:
    with open("publications/rmlm_" + k + ".html", "w") as fh:
        fh.write('<ol reversed id="publications">\n')
        for p in publications:
            if p.kind == k:
                fh.write("<li>\n")
                txt = p.format()
                for mn in my_name:
                    txt = txt.replace(mn, "<u>" + mn + "</u>")
                fh.write(txt)
                fh.write("</li>\n")
        fh.write("</ol>\n")

"""
# write a temporary .html file to check the formatting
with open("test.html", "w") as f:
    f.write('<meta charset="utf-8">\n')
    f.write(
        '<link rel="stylesheet" href="https://cdn.rawgit.com/jpswalsh/academicons/master/css/academicons.min.css">\n'
    )
    f.write("<ol reversed>\n")
    for p in publications:
        if p.kind == "article" or p.kind == "proceeding" or p.kind == "preprint":
            # print(p.date.isoformat())
            f.write("<li>\n")
            f.write(p.format())
            f.write("</li>\n")
    f.write("</ol>\n")
"""

# only ouput list of selected publications
with open("publications/rmlm_selected.html", "w") as fh:
    fh.write('<ol id="publications">\n')
    for p in publications:
        if p.selected == True:
            fh.write("<li>\n")
            txt = p.format()
            for mn in my_name:
                txt = txt.replace(mn, "<u>" + mn + "</u>")
            fh.write(txt)
            fh.write("</li>\n")
    fh.write("</ol>\n")

"""
dates = [p.date for p in publications]
v = [1 for p in publications]
journals = [p.journal for a in publications]

fig, ax = plt.subplots(1, 1)  # constrained_layout=True
# ax.axhline(0, linestyle='-', color='black', zorder=0)
ax.plot(dates, v, "o")

for d, y, j in zip(dates, v, journals):
    ax.annotate(j, (d, y), (d, y), va="bottom", ha="left", rotation=60)

ax.set_ylim(0, 10)
ax.set_xlabel("Date")
# ax.get_xaxis().set_major_locator(mdates.MonthLocator(interval=1))
# ax.get_xaxis().set_major_formatter(mdates.DateFormatter("%Y-%m"))
fig.autofmt_xdate()

fig.tight_layout()
"""

# get only the journal articles
ja_sub_dates = []
ja_pub_dates = []
ja_days2pub = []
ja_journals = []
for p in publications:
    if p.kind == "article" or p.kind == "proceeding":
        ja_sub_dates.append(p.sub_date)
        ja_pub_dates.append(p.pub_date)
        ja_days2pub.append(p.days2publish())
        ja_journals.append(p.journal)
        # print(p.n_authors())

ja_sub_dates = np.array(ja_sub_dates)
ja_pub_dates = np.array(ja_pub_dates)
ja_days2pub = np.array(ja_days2pub)
ja_journals = np.array(ja_journals)

figh, axh = plt.subplots(1, 1, constrained_layout=True)

s = pd.Series(ja_journals)
v = s.sort_values().value_counts()
v.plot(kind="bar")
# axh.yaxis.set_major_formatter(FormatStrFormatter('%d'))
axh.set_ylabel("# of publications")
# axh.set_xlabel("Journal")
# axh.set_title("Where do β-NMR articles get published?")

for tick in axh.get_xticklabels():
    tick.set_rotation(90)

plt.show()
