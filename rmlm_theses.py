# formatted using black

from bibliography import bib

# bnmr theses
theses = [
    bib(
        author="R. M. L. McFadden",
        title="Microscopic dynamics of isolated lithium in crystalline solids revealed by nuclear magnetic relaxation and resonance of <sup>8</sup>Li",
        kind="thesis",
        degree="PhD",
        school="University of British Columbia",
        address="Vancouver",
        year="2020",
        month="04",
        day="24",
        doi="10.14288/1.0389972",
        open_access=True,
        sub_date="2020-04-23",
        pub_date="2020-04-24",
    ),
]
